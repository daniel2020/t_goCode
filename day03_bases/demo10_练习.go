package main

import "fmt"

func main()  {
	/*
	fmt.Printf("")
	%d, 整型
	%f,浮点
	%s,字符串
	%T,数据类型
	%c,%q,数值对应的字符
	%t,bool
	%p,地址
	%v,原始类型
	 */
	a := 100 //int
	b := 3.14 //float64
	c := "hello"//string
	d := `王二狗住在隔壁`//string
	e := true//bool
	f := 'A'//int32

	fmt.Printf("%T,%d\n",a,a)
	fmt.Printf("%T,%f\n",b,b)
	fmt.Printf("%T,%s\n",c,c)
	fmt.Printf("%T,%s\n",d,d)
	fmt.Printf("%T,%t\n",e,e)
	fmt.Printf("%T,%d,%c,%q\n",f,f,f,f)
	fmt.Println("------------------------------")
	fmt.Printf("%v,%v,%v,%v,%v,%v\n",a,b,c,d,e,f)
}
