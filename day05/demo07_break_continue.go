package main

import "fmt"

func main() {
	/*
	循环结束：
		循环条件不满足，循环自动结束
		但是可以通过break和continue来强制结束循环，无论循环条件是否成立。
		break,
		continue,
	 */
	for i := 1; i <= 10; i++ {
		if i == 5{
			//break
			continue
		}
		fmt.Println(i)
	}

	fmt.Println("main...over...")
}
