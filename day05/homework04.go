package main

import "fmt"

func main() {
	/*
	求1-100内，奇数的和
	odd，奇数
	even，偶数
	 */

	 sum :=0
	 for i:=1;i<=100;i++{
	 	if i % 2 != 0{
	 		sum += i
		}
	 }
	 fmt.Println(sum)

	 sum = 0
	 for i:=1;i<=100;i+=2{
	 	sum += i
	 }
	 fmt.Println(sum)
}
