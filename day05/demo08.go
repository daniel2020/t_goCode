package main

import "fmt"

func main() {
	/*
练习1：打印输出1-100内，能被3整除，但是不能被5整除的数，只打印输出前5个即可。
练习2：素数题目中，可否使用break或continue
练习3：多层循环嵌套，break，continue，结束是哪一层？
 */
	count := 0
	for i := 1; i <= 100; i++ {
		if i%3 == 0 && i%5 != 0 {
			fmt.Println(i)
			count++
			if count == 5 {
				break
			}
		}
	}

	fmt.Println("--------------------------------")
	out:for i := 1; i <= 5; i++ {

		for j := 1; j <= 5; j++ {
			if j == 2 {
				//break out //结束指定的某一个循环
				//break//结束的是里层循环：结束整个里层循环
				continue out
				//continue //结束的是里层循环：结束这一次i

			}
			fmt.Printf("i:%d,j:%d\n", i, j)
		}

	}

	a := 10//main里
	if a := 20; a > 0 {//if里
		fmt.Println(a)//20
	}
	fmt.Println(a)//10
}

/*
i	j
1	1
1	2
1	3
1	4
1	5
2	1
2	2
2	3
2	4
2	5

...
 */
