package main

import "fmt"

func main() {
	/*
	 乘法表：									行
 1×1=1									1
 1×2=2 2×2=4							2
 1×3=3 2×3=6 3×3=9						3
 。。。
 1×9=9 2×9=18 9×3=27.。。。。9×9=81

	i，j
	 */
	 //fmt.Println("Helloworld..")

	for i := 1; i < 10; i++ {
		for j := 1; j <= i; j++ {
			fmt.Printf("%d * %d = %d\t", j, i, i*j)
		}
		fmt.Println()
	}

}

/*
调试：debug
	断点
 */
