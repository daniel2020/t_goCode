package main

import "fmt"

func main(){
	/*
	模拟登录，键盘上输入用户名和密码，
		如果用户名是admin密码是123，或者用户名是zhangsan密码是zhangsan123，都表示可以登录。
		否则打印登录失败
	 */

	 var username,password string

	 fmt.Println("请输入用户名：")
	 fmt.Scan(&username)
	 fmt.Println("请输入密码：")
	 fmt.Scanln(&password)
	 if username == "admin" && password == "123" || username == "zhangsan" && password == "zhangsan123"{
	 	fmt.Println(username,",登陆成功")
	 }else{
	 	fmt.Println("用户名或密码错误。。")
	 }

}
