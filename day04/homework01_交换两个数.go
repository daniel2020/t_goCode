package main

import "fmt"

func main(){
	/*
	交换两个数
	 */
	 a := 3
	 b := 2
	 fmt.Printf("a:%d,b:%d\n",a,b)
	//方法一：
	 temp := a // a=3,b=2,temp=3
	 a = b // a=2,b=2,temp=3
	 b = temp // a =2,b=3,temp=3
	 fmt.Printf("a:%d,b:%d\n",a,b)

	 //方法二：
	 a = 3
	 b = 2
	 a, b = b, a// go,python
	fmt.Printf("a:%d,b:%d\n",a,b)

	//方法三：
	a = 3
	b = 2
	a = a + b //a=5,b=2
	b = a - b //a=5,b=3
	a = a - b//a=3,b=3
	fmt.Printf("a:%d,b:%d\n",a,b)

	//方法四
	a = 3
	b = 2
	a = a ^ b
	b = a ^ b
	a = a ^ b
	fmt.Printf("a:%d,b:%d\n",a,b)
}
