package main

import "fmt"

func main(){
	/*
	选择结构：分支语句
		1.if 条件{
		}
	 */
	 a := 5

	 if a > 10{// 为True，一定会执行
	 	fmt.Println(a,"，大于10")
	 }


	 //练习2：给定一个数值，输出他的绝对值
	 b := -5
	 if b < 0{
	 	b = -b
	 }
	 fmt.Println(b)
	 fmt.Println("main...over....")
}
/*
练习1：给定一个年龄，如果大于22岁，输出可以娶媳妇啦
练习2：给定一个数值，输出他的绝对值
练习3：给定一个成绩，如果大于等于60，输出及格
 */
