package main

import "fmt"

func main()  {
	num := 3
	if num > 0 {
		fmt.Println("num是正数。。。")
	}

	fmt.Println(num)

	/*
	if变形写法：
	if 初始化的语句;条件{

	}
	 */
	 if num2 := 5;num>0{//num2的作用域是if语句，if语句结束，num2随之销毁
	 	fmt.Println("num2是正数。。。",num2)
	 }

	 //fmt.Println(num2)//undefined: num2
}
