package main

import "fmt"

func main()  {
	/*
	switch  变量{
	case 数值1：
		分支1
	case 数值2：
		分支2

	default：

	}

	break,强制结束switch，结束了case的执行

	fallthrouth,穿透
		当某个case匹配成功后执行，如果有fallthrough，那么后面紧邻的case不再匹配，直接执行
	 */
	 a := 1
	switch a {
	case 1:
		fmt.Println("出演熊大")
		fmt.Println("出演熊大")
		//break
		fmt.Println("出演熊大")
		fmt.Println("出演熊大")
		fallthrough
	case 2:
		fmt.Println("出演熊二")
		fmt.Println("出演熊二")
		fmt.Println("出演熊二")
		fmt.Println("出演熊二")
		fallthrough
	case 3:
		fmt.Println("光头强。。")
		fmt.Println("光头强。。")
		fmt.Println("光头强。。")
		fmt.Println("光头强。。")
		fmt.Println("光头强。。")
	default:
		fmt.Println("我也不知道你演啥。。")
	}

	month := 5
	day := 0
	switch month {
	case 1:
		fallthrough
	case 3:
		fallthrough
	case 5:
		fallthrough
	case 7:
		fallthrough
	case 8:
		fallthrough
	case 10:
		fallthrough
	case 12:
		day = 31
	case 4:
		fallthrough
	case 6:
		fallthrough
	case 9:
		fallthrough
	case 11:
		day = 30
	case 2:
		day = 28
	}
	fmt.Println(month,day)
}
