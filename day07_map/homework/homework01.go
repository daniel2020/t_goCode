package main

import (
	"fmt"
	"math"
)

func main() {
	/*
	题目：某个公司采用公用电话传递数据，数据是四位的整数，在传递过程中是加密的，加密规则如下：
	每位数字都加上5,然后用和，除以10的余数代替该数字，再将第一位和第四位交换，第二位和第三位交换。

	1694
	1->6->6
	6->11->1
	9->14->4
	4->9->9

	9416
	 */


	num := 0
	 for {
	 	fmt.Println("请输入一个4位整数：")
		fmt.Scanln(&num) // 12345
		if num >=1000 && num < 10000{
			break
		}
		fmt.Println("听话，输入4位数：")
	 }


	arr := [4]int{}
	for i := 0; i < len(arr); i++ {
		/*
		i=0, % 10000, / 1000
		i=1, % 1000, / 100
		i=2, % 100, /10
		 */
		arr[i] = num % int(math.Pow10(4-i)) / int(math.Pow10(3-i))
	}
	//qian := num % 10000 / 1000
	//bai := num % 1000 / 100
	//shi := num % 100 / 10
	//ge := num % 10 / 1

	//arr := [4]int{qian, bai, shi, ge}
	//fmt.Println(arr)
	for i := 0; i < len(arr); i++ {
		arr[i] += 5
		arr[i] %= 10

	}
	//fmt.Println(arr)
	for i := len(arr) - 1; i >= 0; i-- {
		fmt.Print(arr[i])
	}

}
