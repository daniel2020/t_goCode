package main

import "fmt"

func main() {
	/*
	有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），凡报到3的人退出圈子，
	问最后留下1个人的是原来第几号的那位。
	 */
	const n  = 7
	 count := 0//计数器
	 sum := 0//推出的人数
	 index := 0//访问数组的下标
	 arr := [n]bool{}
	 for i:=0;i<len(arr);i++{
	 	arr[i] = true
	 }
	 fmt.Println(arr)


	 //循环
	 for sum < 6{
	 	//arr[0],arr[1],arr[2],arr[3],arr[4],arr[5],arr[6],arr[0]
	 	if arr[index]{//== true
	 		count++ //1,2,3
	 		if count == 3{
	 			count =0
	 			arr[index] =false
	 			sum++ // 1,2
			}
		}
		index++ //[0,6]  1,2,3,4,5,6,7, 0
		if index == n{//处理边界
			index = 0//重新置为0
		}
	 }
	 fmt.Println(arr)
	 for i:=0;i<len(arr);i++{
	 	if arr[i] == true{
	 		fmt.Println(i+1)
		}
	 }
}
