package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	/*
	给定一个整型数组，长度为10。数字取自随机数。
	[1,10]
	 */

	 arr :=[10]int{}
	 fmt.Println(arr)

	 rand.Seed(time.Now().UnixNano())
	 for i:=0;i<len(arr);i++{
	 	x := 0
	 	//验证：x是否已经存储过了
	 	/*
	 	 循环验证x是否已经存储，如果已经有，再取随机数，再验证，直到不重复
	 	 */
	 	 for{
	 	 	x = rand.Intn(10)+1 // 8
	 	 	fmt.Printf("i:%d,x:%d\n",i,x)
	 	 	flag := true//true：代表不重复值可以用，false代表值重复，不可以使用
	 	 	for j:=0;j<i;j++{
	 	 		if x == arr[j]{
	 	 			flag = false
	 	 			break
				}
			}
			if flag {
				//判断标记的值
				break
			}

		 }
	 	arr[i] = x
	 }
	 fmt.Println(arr)
}
/*
[0 0 0 0 0 0 0 0 0 0]
[4 0 0 0 0 0 0 0 0 0]
[4 9 0 0 0 0 0 0 0 0]
[4 9 7 0 0 0 0 0 0 0]
8
 */
